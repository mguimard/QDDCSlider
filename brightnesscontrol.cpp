#include "brightnesscontrol.h"
#include "ui_brightnesscontrol.h"

using namespace std::chrono_literals;

BrightnessControl::BrightnessControl(char * name, DDCA_Display_Handle& ddca_dh_loc, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BrightnessControl),
    ddca_dh_loc(ddca_dh_loc)
{
    ui->setupUi(this);

    ui->label->setText(name);

    DDCA_Non_Table_Vcp_Value valrec;

    DDCA_Status status = ddca_get_non_table_vcp_value(ddca_dh_loc, 0x10, &valrec);
    //printf("status %d\n", status);
    //printf("mh %d \n", valrec.mh);
    //printf("sh %d \n", valrec.sh);
    //printf("ml %d \n", valrec.ml); // max value
    //printf("sl %d \n", valrec.sl); // real value

    ui->horizontalSlider->setValue(valrec.sl);

    keepalive_thread_run = true;

    update_thread = new std::thread(&BrightnessControl::UpdateThread, this);

    connect(ui->horizontalSlider, &QSlider::valueChanged, this, [=](int val){
        new_val = val;
    });
}

BrightnessControl::~BrightnessControl()
{
    keepalive_thread_run = false;
    update_thread->join();
    delete update_thread;

    delete ui;
}

void BrightnessControl::UpdateThread()
{
    while(keepalive_thread_run.load())
    {
        int o = last_val.load();
        int v = new_val.load();

        if(o != v)
        {
            last_val = v;
            //printf("value %d\n", v);
            DDCA_Status status = ddca_set_non_table_vcp_value(ddca_dh_loc, 0x10, 0, v);
            //printf("status %d\n", status);
        }

        std::this_thread::sleep_for(100ms);
    }
}
