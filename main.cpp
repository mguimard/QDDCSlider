#include "mainwindow.h"

#include <QApplication>
#include <QWidgetAction>
#include <QLabel>
#include <QMenu>
#include <QLayout>
#include <QSystemTrayIcon>
#include <QStyleFactory>
#include <brightnesscontrol.h>
#include <ddcutil_c_api.h>

int main(int argc, char *argv[])
{
    DDCA_Display_Info_List*  dlist_loc;
    DDCA_Status status = ddca_get_display_info_list2(false, &dlist_loc);

    //printf("status %d\n", status);

    for(int i = 0; i < dlist_loc->ct; i++)
    {
        DDCA_Display_Info info = dlist_loc->info[i];

        //printf("%s\n", info.model_name);

        DDCA_Display_Handle ddca_dh_loc;

        status = ddca_open_display2(info.dref, true, &ddca_dh_loc);
        //printf("status %d\n", status);

        char* caps_loc;
        status = ddca_get_capabilities_string(ddca_dh_loc, &caps_loc);
        //printf("status %d\n", status);
        //printf("caps_loc: %s\n", caps_loc);


        DDCA_Capabilities * parsed_capabilities_loc;
        status = ddca_parse_capabilities_string(caps_loc, &parsed_capabilities_loc);

        //printf("status %d\n", status);

        bool brightness_support = false;

        for(int c = 0; c < parsed_capabilities_loc->vcp_code_ct; c++)
        {
            if(parsed_capabilities_loc->vcp_codes[c].feature_code == 0x10)
            {
                brightness_support = true;
            }
        }

        if(brightness_support)
        {
            //printf("Support brightness \n");

            QApplication a(argc, argv);
            MainWindow w;
            QSystemTrayIcon trayIcon;
            QWidgetAction* pWidgetAction = new QWidgetAction(nullptr);
            QMenu menu(pWidgetAction->parentWidget());
            BrightnessControl* c = new BrightnessControl(info.model_name, ddca_dh_loc, menu.parentWidget());

            pWidgetAction->setDefaultWidget(c);
            menu.addAction(pWidgetAction);
            trayIcon.setContextMenu(&menu);
            trayIcon.setIcon(QIcon(":/icon.png"));
            trayIcon.show();

            QWidget::connect(&trayIcon, &QSystemTrayIcon::activated, &w, [&](){
                menu.popup(QCursor::pos());
            });

            return a.exec();
        }
    }

    printf("No display found\n");
    return -1;
}
