# What is it ?

This is a simple tray slider to control your DDC-CI monitor brightness.

# Prerequistes :

- libddcutil-dev<br/>
- libddcutil4<br/>
- qtcreator

Debian based distribution :
```
apt update && apt install libddcutil-dev libddcutil4 qtcreator
```



# How to build :

- Clone the sources<br/>
- Open the project file in QT Creator
- Build
