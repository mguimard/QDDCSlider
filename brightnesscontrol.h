#ifndef BRIGHTNESSCONTROL_H
#define BRIGHTNESSCONTROL_H

#include <QWidget>
#include <ddcutil_c_api.h>
#include <thread>

namespace Ui {
class BrightnessControl;
}

class BrightnessControl : public QWidget
{
    Q_OBJECT

public:
    explicit BrightnessControl(char * name, DDCA_Display_Handle& ddca_dh_loc, QWidget *parent = nullptr);
    ~BrightnessControl();

private:
    Ui::BrightnessControl *ui;

    std::thread* update_thread;
    std::atomic<bool> keepalive_thread_run;
    std::atomic<int>  last_val = 0;
    std::atomic<int>  new_val = 0;
    void UpdateThread();
    DDCA_Display_Handle ddca_dh_loc;

};

#endif // BRIGHTNESSCONTROL_H
